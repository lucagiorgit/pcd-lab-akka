package pcd.actors.ex1_prodcons

import akka.actor.{Actor, ActorRef, ActorSystem, Address, Deploy, Props}
import akka.event.Logging
import akka.remote.RemoteScope
import akka.routing.{RandomRoutingLogic, RoundRobinRoutingLogic, Router, RoutingLogic}
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._

case object DoProduce

case class Consume[T](value: T)

case class Product[T](value: T)

case class AddConsumer(ref: ActorRef)

case object Start

case object Schedule

case class AddSchedulable(ref: ActorRef)

class Producer[T](supplier: () => T, to: ActorRef) extends Actor {
  var no = 0
  val log = Logging(context.system, this)

  override def receive: Receive = {
    case DoProduce =>
      no = no + 1
      val item = supplier()
      log.debug(s"I am producing $item (item#$no)")
      to ! Product(item)
  }
}

object Producer {
  def props[T](supplier: () => T, to: ActorRef) = Props(classOf[Producer[T]], supplier, to)
}

class Consumer[T, R](consumer: (T) => R) extends Actor {
  var no = 0
  val log = Logging(context.system, this)

  override def receive: Receive = {
    case Consume(v: T) =>
      no = no + 1
      // Thread.sleep(10000)
      log.debug(s"I am consuming $v (item#$no)")
      consumer(v)
  }
}

object Consumer {
  def props[T, R](consumer: (T) => R) = Props(classOf[Consumer[T, R]], consumer)
}

class LoadBalancer(routingLogic: RoutingLogic) extends Actor {
  val log = Logging(context.system, this)

  var router = Router(routingLogic)

  override def preStart(): Unit =
    log.debug("Pre-start")

  override def receive: Receive = {
    case AddConsumer(c) => router = router.addRoutee(c)
    case Product(p) => router.route(Consume(p), sender())
  }
}

object LoadBalancer {
  def props(rl: RoutingLogic) = Props(classOf[LoadBalancer], rl)
}

class Scheduler(period: FiniteDuration) extends Actor {
  private var router = Router(RoundRobinRoutingLogic())

  override def receive: Receive = basicBehavior.orElse { case Start => start() }

  def basicBehavior: Receive = {
    case AddSchedulable(s) => router = router.addRoutee(s)
  }

  def operationBehavior: Receive = {
    case Schedule => {
      router.route(DoProduce, self)
    }
  }

  private def start() = {
    context.become(operationBehavior)
    context.system.scheduler.schedule(period, period, self, Schedule)(context.dispatcher)
  }
}

object Scheduler {
  def props(period: FiniteDuration = 500 millis) = Props(classOf[Scheduler], period)
}

/*
 Notice
 - Each actor is quite simple (SRP)
 - Companion object of each actor class defines the "factory" props
 - Levels of indirection in the system (decoupling between actors)
 - Use of routers
 - Use of system scheduler to self-sending a message (Scheduler)
 - Regarding behaviour composition via partialfunction chaining and "becomes" see also ex2

 Two questions:
 1) What happens if consumers block?
 2) How would you modify the system to handle backpressure?
 */
object ProducersConsumersApp extends App {
  val system = ActorSystem("MySystem", ConfigFactory.parseString("""akka.loglevel = "DEBUG""""))
  val loadBalancer = system.actorOf(LoadBalancer.props(RandomRoutingLogic()), "LoadBalancer")
  val scheduler = system.actorOf(Scheduler.props(50 millis), "Scheduler")
  val producers = (1 to 10) map { i => system.actorOf(Producer.props(() => (Math.random() * 100).toInt, loadBalancer), s"Producer$i") }
  val consumers = (1 to 50) map { i => system.actorOf(Consumer.props((v: Int) => println(v)), s"Consumer$i") }
  producers.foreach(p => scheduler ! AddSchedulable(p))
  consumers.foreach(c => loadBalancer ! AddConsumer(c))
  scheduler ! Start
}