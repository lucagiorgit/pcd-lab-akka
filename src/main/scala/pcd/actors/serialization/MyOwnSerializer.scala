package pcd.actors.serialization

import java.nio.charset.StandardCharsets

import akka.routing.RandomRoutingLogic
import akka.serialization.SerializerWithStringManifest

class MyOwnSerializer extends SerializerWithStringManifest {
  val RandomRoutingLogicManifest = "RandomRoutingLogic"
  val UTF_8 = StandardCharsets.UTF_8.name()

  def identifier = 1234567

  def manifest(obj: AnyRef): String =
    obj match {
      case _: RandomRoutingLogic => RandomRoutingLogicManifest
    }

  def toBinary(obj: AnyRef): Array[Byte] = obj match {
    case r: RandomRoutingLogic => RandomRoutingLogicManifest.getBytes()
  }

  def fromBinary(bytes: Array[Byte], manifest: String): AnyRef = manifest match {
    case RandomRoutingLogicManifest => RandomRoutingLogic()
  }
}