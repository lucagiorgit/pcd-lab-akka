package pcd.actors.ex2_observables

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.event.Logging
import akka.util.Timeout
import com.typesafe.config.ConfigFactory

import scala.concurrent.Future
import scala.concurrent.duration._

case class AddObserver(actor: ActorRef)
case class RemoveObserver(actor: ActorRef)
case class Notification[T](item: T, by: ActorRef)

trait ObservableActor[T] { thisActor: Actor =>
  var observers: Set[ActorRef] = Set()

  val observableBehaviour: Receive = {
    case AddObserver(obs) => observers += obs
    case RemoveObserver(obs) => observers -= obs
  }

  def notifyObservers(item: T) =
    observers.foreach(_ ! Notification(item, self))
}

trait CounterMsg
case object Up extends CounterMsg
case object Down extends CounterMsg
case object Inspect extends CounterMsg

case class CounterState(k: Int, direction: String)

class Counter(private var k: Int, private val bottom: Int, private val top: Int) extends Actor with ObservableActor[Any] {
  require(k>=bottom && k<=top)
  require(bottom < top)

  val inspectBehaviour: Receive = {
    case Inspect => sender ! state
  }

  val upBehaviour: Receive = {
    case Up if k<top =>
      k += 1
      if(k==top) {
        notifyObservers(state)
        become(downBehaviour)
      }
  }

  val downBehaviour: Receive = {
    case Down if k>bottom =>
      k -= 1
      if(k==bottom){
        notifyObservers(state)
        become(upBehaviour)
      }
  }

  def become(next: Receive) =
    context.become(next orElse inspectBehaviour orElse observableBehaviour)

  val startBehaviour: Receive =
    (if(k<top) upBehaviour else downBehaviour) orElse inspectBehaviour orElse observableBehaviour

  override def receive: Receive = startBehaviour

  override def unhandled(message: Any): Unit = notifyObservers("unhandled")

  private def state = CounterState(k, if(receive.isDefinedAt(Up)) "up" else "down")
}

object Counter {
  def props(start: Int = 0, bottom: Int = Int.MinValue, top: Int = Int.MaxValue) = Props(classOf[Counter], start, bottom, top)
}

class CounterObserver extends Actor {
  val log = Logging(context.system, this)

  override def receive: Receive = {
    case Notification(CounterState(k,direction), _) => log.debug(s"Observing $k $direction")
    case Notification("unhandled", _) => log.debug("Counter cannot handle message")
  }
}

/**
  * Elements to notice:
  * - Composition of behaviours
  * - Unhandled message hook
  * - Ask pattern below
  */
object ObservableCounterApp extends App {
  val system = ActorSystem("MySystem", ConfigFactory.parseString("""akka.loglevel = "DEBUG""""))

  val counter = system.actorOf(Counter.props(0,0,10), "counter")
  val observer = system.actorOf(Props(classOf[CounterObserver]), "observer")

  counter ! AddObserver(observer)

  Thread.sleep(500)

  (1 to 12) foreach { _ => counter ! Up }
  Thread.sleep(1000)
  (1 to 6) foreach { _ => counter ! Down }

  import akka.pattern.ask
  implicit val timeout: Timeout = Timeout(500 millis)

  //   val future: Future[CounterState] = (counter ask Inspect).mapTo[CounterState]
  val future: Future[CounterState] = (counter ? Inspect).mapTo[CounterState]

  future.onSuccess{
    case CounterState(k, direction) => println(s"Counter is in state ($k,$direction)")
  }(system.dispatcher)

  Thread.sleep(1000)
  (1 to 6) foreach { _ => counter ! Down }
}
